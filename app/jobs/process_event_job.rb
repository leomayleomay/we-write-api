class ProcessEventJob < ApplicationJob
  queue_as :default

  def perform(guid, name, payload)
    return if ['ping', 'push', 'create', 'delete'].include?(name)

    payload = JSON.parse(payload, symbolize_names: true)

    if ['pull_request', 'repository', 'star', 'fork', 'issue_comment'].include?(name)
      "Events::#{name.classify}".safe_constantize.new(payload).process
    else
      ActionMailer::Base.mail(
        to: "leomayleomay@gmail.com",
        from: "no-reply@wewrite.works",
        subject: "[Unknown Event] #{guid}/#{name}",
        body: payload
      ).deliver_now
    end
  rescue Events::FailedToProcess => e
    ActionMailer::Base.mail(
      to: "leomayleomay@gmail.com",
      from: "no-reply@wewrite.works",
      subject: "[Failed to process] #{guid}/#{name}",
      body: payload
    ).deliver_now
  end
end
