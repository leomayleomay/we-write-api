class CreateHookJob < ApplicationJob
  queue_as :default

  def perform(repo)
    owner = repo.user

    Octokit::Client.new(access_token: owner.github_token).tap do |client|
      client.create_hook("#{owner.login}/#{repo.name}", 'web', {
        url: Rails.application.routes.url_helpers.events_url,
        secret: Rails.application.credentials.dig(:github, :webhook_secret),
        content_type: 'json',
        insecure_ssl: '0'
      }, {
        events: ['*'],
        active: true
      })
    end
  end
end