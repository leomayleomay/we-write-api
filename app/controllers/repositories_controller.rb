class RepositoriesController < ApplicationController
  def index
    @repositories = Repository.includes(:user).order(stargazers_count: :desc, forks_count: :desc)
  end
end