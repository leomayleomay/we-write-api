class EventsController < ApplicationController
  before_action :verify_signature!

  def create
    ProcessEventJob.perform_later(guid, name, payload)

    head :ok
  end

  protected

  def verify_signature!
    actual_signature = request.headers['X-Hub-Signature']

    expected_signature = 'sha1=' + OpenSSL::HMAC.hexdigest(
      OpenSSL::Digest.new('sha1'),
      Rails.application.credentials.dig(:github, :webhook_secret),
      payload
    )

    if !Rack::Utils.secure_compare(actual_signature, expected_signature)
      return head :internal_server_error
    end
  end

  def guid
    request.headers['X-GitHub-Delivery']
  end

  def name
    request.headers['X-GitHub-Event']
  end

  def payload
    @payload ||= request.body.string
  end
end