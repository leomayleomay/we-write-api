class OmniauthCallbacksController < ApplicationController
  def github
    @user = User.from_omniauth(request.env["omniauth.auth"])

    if @user.persisted?
      redirect_to Figaro.env.frontend_host + "/auth/callback?token=" + @user.token
    else
      redirect_to Figaro.env.frontend_host + "/auth/failure"
    end
  end

  def failure
    redirect_to Figaro.env.frontend_host + "/auth/failure"
  end
end