class ApplicationController < ActionController::API

  protected

  def current_user
    @current_user
  end

  def authenticate_user!
    begin
      JsonWebToken.decode(token).tap do |decoded|
        @current_user = User.find(decoded[:id])
      end
    rescue ActiveRecord::RecordNotFound, JWT::DecodeError => e
      render json: { error: "Invalid token" }, status: :unauthorized
    end
  end

  def token
    if header = request.headers['Authorization']
      header.split(' ').last
    end
  end
end
