json.array! @repositories do |repository|
  json.id repository.id
  json.name repository.name
  json.description repository.description
  json.forks_count repository.forks_count
  json.stargazers_count repository.stargazers_count
  json.owner do
    json.name repository.user.name
    json.login repository.user.login
    json.image repository.user.image
  end
end