module Events
  class Star < Base
    def process
      repository.update_attributes!(
        stargazers_count: payload.dig(:repository, :stargazers_count)
      )
    end
  end
end