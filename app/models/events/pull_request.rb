module Events
  class PullRequest < Base
    def process
      case payload.fetch(:action)
      when 'opened' then
        ActionMailer::Base.mail(
          to: repository.user.email,
          from: "no-reply@wewrite.works",
          subject: "#{sender} just opened a review",
          body: "Please find out the details in #{pull_request_url}"
        ).deliver_now
      when 'closed' then return
      else
        raise FailedToProcess
      end
    end

    protected

    def pull_request_url
      payload.dig(:pull_request, :html_url).gsub("https://github.com", Figaro.env.frontend_host)
    end

    def sender
      payload.dig(:sender, :login)
    end
  end
end