module Events
  class Repository < Base
    def process
      case payload.fetch(:action)
      when 'created'
      when 'deleted'
      when 'archived'
      when 'unarchived'
      when 'renamed'
      when 'privatized'
      when 'transferred' then return
      when 'edited' then
        repository.update_attributes!(
          payload.dig(:repository).slice(:name, :description)
        )
      when 'publicized' then
        user = User.find_by!(uid: payload.dig(:sender, :id))

        return if user.repositories.exists?(node_id: payload.dig(:repository, :node_id))

        user.repositories.create!(
          payload.dig(:repository).slice(:name, :description, :node_id, :forks_count, :stargazers_count)
        ).tap do |repository|
          CreateHookJob.perform_now(repository)

          ActionMailer::Base.mail(
            to: User.where.not(uid: user.uid).pluck(:email),
            from: "no-reply@wewrite.works",
            subject: "#{user.login} just published <#{repository.name}>!",
            body: "Please find out the details in #{repository_url}"
          ).deliver_now
        end
      else
        raise FailedToProcess
      end
    end

    protected

    def repository_url
      payload.dig(:repository, :html_url).gsub('https://github.com', Figaro.env.frontend_host)
    end
  end
end