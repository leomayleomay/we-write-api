module Events
  class IssueComment < Base
    def process
      case payload.fetch(:action)
      when 'created'
        ActionMailer::Base.mail(
          to: repository.user.email,
          from: "no-reply@wewrite.works",
          subject: "#{sender} just commented your works!",
          body: "Please find out the details in #{html_url}"
        ).deliver_now
      when 'deleted' then return
      else
        raise FailedToProcess
      end
    end

    protected

    def sender
      payload.dig(:sender, :login)
    end

    def html_url
      payload.dig(:issue, :html_url).gsub("https://github.com", Figaro.env.frontend_host)
    end
  end
end