module Events
  class Fork < Base
    def process
      user = User.find_by!(
        uid: payload.dig(:sender, :id)
      )

      user.repositories.create!(
        payload.dig(:forkee).slice(:name, :description, :node_id, :forks_count, :stargazers_count)
      ).tap do |repo|
        CreateHookJob.perform_now(repo)
      end

      repository.update_attributes!(
        forks_count: payload.dig(:repository, :forks_count)
      )

      ActionMailer::Base.mail(
        to: repository.user.email,
        from: "no-reply@wewrite.works",
        subject: "#{user.login} just forked your works!",
        body: "Congrats!"
      ).deliver_now
    end
  end
end