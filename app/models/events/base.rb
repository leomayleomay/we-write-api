module Events
  class FailedToProcess < StandardError; end

  class Base
    attr_accessor :payload

    def initialize(payload)
      @payload = payload
    end

    def process
      # no-op
    end

    protected

    def repository
      @repository ||= ::Repository.find_by!(payload.fetch(:repository).slice(:node_id))
    end
  end
end