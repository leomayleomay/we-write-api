class Repository < ApplicationRecord
  belongs_to :user

  validates :name, :forks_count, :stargazers_count, :user, presence: true
  validates :node_id, presence: true, uniqueness: true
end
