class User < ApplicationRecord
  validates :provider, presence: true, uniqueness: { scope: [:uid] }
  validates :uid, presence: true, uniqueness: { scope: [:provider] }
  validates :login, :github_token, presence: true, if: :github?

  has_many :repositories, dependent: :destroy

  def self.from_omniauth(auth)
    user = find_or_initialize_by(provider: auth.provider, uid: auth.uid)

    user.name = auth.info.name
    user.email = auth.info.email
    user.image = auth.info.image

    if auth.provider == 'github'
      user.login = auth.extra.raw_info.login
      user.github_token = auth.credentials.token
    end

    user.save

    user
  end

  def token
    JsonWebToken.encode({
      id: id,
      github_token: github_token
    })
  end

  def github?
    provider.presence == 'github'
  end
end