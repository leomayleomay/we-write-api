module JsonWebToken
  extend self

  def encode(payload)
    JWT.encode(payload, secret, 'HS256')
  end

  def decode(token)
    JWT.decode(token, secret, 'HS256')[0].with_indifferent_access
  end

  protected

  def secret
    Rails.application.credentials.secret_key_base
  end
end