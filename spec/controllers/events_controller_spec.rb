require 'rails_helper'

describe EventsController, type: :controller, perform_enqueued: true do
  describe 'POST /create' do
    context 'blank signature' do
      before do
        @request.headers['X-Hub-Signature'] = ''
      end

      it 'returns 500' do
        post :create, body: ''

        expect(response.status).to eq 500
      end
    end

    context 'invalid signature' do
      before do
        @request.headers['X-Hub-Signature'] = '123'
      end

      it 'returns 500' do
        post :create, body: ''

        expect(response.status).to eq 500
      end
    end

    context 'valid signature' do
      context 'ping' do
        before do
          @request.headers['X-GitHub-Event'] = 'ping'
          @request.headers['X-Hub-Signature'] = 'sha1=044cf76245b68d6f41267db076b75b8fbd3f55e2'
        end

        let(:body) { File.read(Rails.root.join('spec/fixtures/ping.json')) }

        it 'returns 200' do
          post :create, body: body

          expect(response.status).to eq 200
        end
      end

      context 'push' do
        before do
          @request.headers['X-GitHub-Event'] = 'push'
          @request.headers['X-Hub-Signature'] = 'sha1=5a7182c773816b408972f283757b2e37b67c0226'
        end

        let(:body) { File.read(Rails.root.join('spec/fixtures/push.json')) }

        it 'returns 200' do
          post :create, body: body

          expect(response.status).to eq 200
        end
      end

      context 'star' do
        let!(:user) { create(:user) }
        let!(:repository) { create(:repository, user: user, node_id: "MDEwOlJlcG9zaXRvcnkxODY4NTMwMDI=") }

        before do
          @request.headers['X-GitHub-Event'] = 'star'
          @request.headers['X-Hub-Signature'] = 'sha1=743f1d649bda0029a8c62864f53310deb40b374e'
        end

        let(:body) { File.read(Rails.root.join('spec/fixtures/star.created.json')) }

        it 'returns 200' do
          post :create, body: body

          expect(response.status).to eq 200
        end

        it 'update stargazers_count for the repository' do
          post :create, body: body

          expect(repository.reload.stargazers_count).to eq 1
        end
      end

      context 'fork' do
        let!(:user) { create(:user) }
        let!(:repository) { create(:repository, user: user, node_id: "MDEwOlJlcG9zaXRvcnkxODY4NTMwMDI=") }

        let!(:forker) { create(:user, uid: '38302899') }

        before do
          @request.headers['X-GitHub-Event'] = 'fork'
          @request.headers['X-Hub-Signature'] = 'sha1=1b62c279c51509d6eb3e0214a6b2811871f0469d'
        end

        let(:body) { File.read(Rails.root.join('spec/fixtures/fork.json')) }

        before do
          stub_request(:post, /https:\/\/api.github.com\/repos\/.*\/hooks/).to_return(status: 200, body: "", headers: {})
        end

        it 'returns 200' do
          post :create, body: body

          expect(response.status).to eq 200
        end

        it 'creates a new repository' do
          expect {
            post :create, body: body
          }.to change(forker.repositories, :count).by(1)

          repository = forker.repositories.last
          expect(repository.name).to eq 'Hello-World'
        end

        it 'sets up webhook for new repository' do
          expect(CreateHookJob).to receive(:perform_now)

          post :create, body: body
        end

        it 'updates the forks_count on the original repository' do
          post :create, body: body

          expect(repository.reload.forks_count).to eq 1
        end

        it 'notifies the owner' do
          post :create, body: body

          mail = ActionMailer::Base.deliveries.last

          expect(mail.to).to eq [user.email]
          expect(mail.from).to eq ['no-reply@wewrite.works']
          expect(mail.subject).to eq "#{forker.login} just forked your works!"
          expect(mail.body.decoded).to eq "Congrats!"
        end
      end

      context 'issue comment created' do
        let!(:user) { create(:user) }
        let!(:repository) { create(:repository, user: user, node_id: "MDEwOlJlcG9zaXRvcnkxODY4NTMwMDI=") }

        before do
          @request.headers['X-GitHub-Event'] = 'issue_comment'
          @request.headers['X-Hub-Signature'] = 'sha1=4402377b01cdba4f9e9b4bcf8dd7652a20f01782'
        end

        let(:body) { File.read(Rails.root.join('spec/fixtures/issue_comment.created.json')) }

        it 'returns 200' do
          post :create, body: body

          expect(response.status).to eq 200
        end

        it 'notifies the owner' do
          post :create, body: body

          mail = ActionMailer::Base.deliveries.last

          expect(mail.to).to eq [user.email]
          expect(mail.from).to eq ['no-reply@wewrite.works']
          expect(mail.subject).to eq 'Codertocat just commented your works!'
          expect(mail.body.decoded).to eq "Please find out the details in #{Figaro.env.frontend_host}/Codertocat/Hello-World/issues/1"
        end
      end

      context 'pull request opened' do
        let!(:user) { create(:user) }
        let!(:repository) { create(:repository, user: user, node_id: "MDEwOlJlcG9zaXRvcnkxODY4NTMwMDI=") }

        before do
          @request.headers['X-GitHub-Event'] = 'pull_request'
          @request.headers['X-Hub-Signature'] = 'sha1=18e347d4a98a460c2b71be8058425fba21d96377'
        end

        let(:body) { File.read(Rails.root.join('spec/fixtures/pull_request.opened.json')) }

        it 'returns 200' do
          post :create, body: body

          expect(response.status).to eq 200
        end

        it 'notifies the owner' do
          post :create, body: body

          mail = ActionMailer::Base.deliveries.last

          expect(mail.to).to eq [user.email]
          expect(mail.from).to eq ['no-reply@wewrite.works']
          expect(mail.subject).to eq 'Codertocat just opened a review'
          expect(mail.body.decoded).to eq "Please find out the details in #{Figaro.env.frontend_host}/Codertocat/Hello-World/pull/2"
        end
      end

      context 'repository edited' do
        let!(:user) { create(:user) }
        let!(:repository) { create(:repository, user: user, name: 'nice works', node_id: "MDEwOlJlcG9zaXRvcnkxODY4NTMwMDI=") }

        before do
          @request.headers['X-GitHub-Event'] = 'repository'
          @request.headers['X-Hub-Signature'] = 'sha1=a153995c9c205edce8c3a9ee7f377bb8e0cf608b'
        end

        let(:body) { File.read(Rails.root.join('spec/fixtures/repository.edited.json')) }

        it 'returns 200' do
          post :create, body: body

          expect(response.status).to eq 200
        end

        it 'updates the repository' do
          post :create, body: body

          expect(repository.reload.name).to eq 'Hello-World'
        end
      end

      context 'repository publicized' do
        let!(:author) { create(:user, uid: '21031067') }

        let!(:reader) { create(:user) }

        before do
          @request.headers['X-GitHub-Event'] = 'repository'
          @request.headers['X-Hub-Signature'] = 'sha1=3a5e9df9627ebac03cb4fe4c3b2b17e96f1e7ca4'

          stub_request(:post, /https:\/\/api.github.com\/repos\/.*\/hooks/).to_return(status: 200, body: "", headers: {})
        end

        let(:body) { File.read(Rails.root.join('spec/fixtures/repository.publicized.json')) }

        it 'returns 200' do
          post :create, body: body

          expect(response.status).to eq 200
        end

        context 'no repo found with given node ID' do
          it 'creates a new repository' do
            expect {
              post :create, body: body
            }.to change(author.repositories, :count).by(1)

            repository = author.repositories.last

            expect(repository.name).to eq 'Hello-World'
          end

          it 'sets up webhook for new repository' do
            expect(CreateHookJob).to receive(:perform_now)

            post :create, body: body
          end

          it 'notifies all the readers' do
            post :create, body: body

            mail = ActionMailer::Base.deliveries.last

            expect(mail.to).to eq [reader.email]
            expect(mail.from).to eq ['no-reply@wewrite.works']
            expect(mail.subject).to eq "#{author.login} just published <Hello-World>!"
            expect(mail.body.decoded).to eq "Please find out the details in #{Figaro.env.frontend_host}/Codertocat/Hello-World"
          end
        end

        context 'a repo exists for given node ID' do
          let!(:repo) { create(:repository, user: author, node_id: 'MDEwOlJlcG9zaXRvcnkxODY4NTMwMDI=') }

          it 'does not create new repo' do
            expect {
              post :create, body: body
            }.to change(author.repositories, :count).by(0)
          end

          it 'does not set up webhook' do
            expect(CreateHookJob).not_to receive(:perform_now)

            post :create, body: body
          end

          it 'does not notify readers' do
            post :create, body: body

            expect(ActionMailer::Base.deliveries).to be_empty
          end
        end
      end

      context 'unknown event' do
        before do
          @request.headers['X-GitHub-Event'] = 'gollum'
          @request.headers['X-GitHub-Delivery'] = 'a92b5cb0-8c7e-11e9-8461-e2cdfa76d080'
          @request.headers['X-Hub-Signature'] = 'sha1=6bd94eec43eaa2e284694d09d8716f7471d0af3e'
        end

        let(:body) { File.read(Rails.root.join('spec/fixtures/gollum.json')) }

        it 'returns 200' do
          post :create, body: body

          expect(response.status).to eq 200
        end

        it 'should notify me' do
          post :create, body: body

          mail = ActionMailer::Base.deliveries.last

          expect(mail.to).to eq ["leomayleomay@gmail.com"]
          expect(mail.from).to eq ['no-reply@wewrite.works']
          expect(mail.subject).to eq "[Unknown Event] a92b5cb0-8c7e-11e9-8461-e2cdfa76d080/gollum"
        end
      end
    end
  end
end