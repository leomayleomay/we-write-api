FactoryBot.define do
  factory :user do
    name { "John Doe" }

    provider { "github" }

    sequence :uid do |n|
      n.to_s
    end

    sequence :login do |n|
      "user_#{n}"
    end

    sequence :email do |n|
      "user_#{n}@wewrite.works"
    end

    sequence :github_token do |n|
      "github_token_#{n}"
    end
  end

  factory :repository do
    user

    forks_count { 0 }

    stargazers_count { 0 }

    sequence :name do |n|
      "repository_#{n}"
    end

    sequence :node_id do |n|
      n.to_s
    end
  end
end