class CreateRepositories < ActiveRecord::Migration[6.0]
  def change
    create_table :repositories do |t|
      t.string :name, null: false
      t.string :node_id, null: false
      t.text :description
      t.integer :forks_count, null: false, default: 0
      t.integer :stargazers_count, null: false, default: 0
      t.belongs_to :user, index: true, null: false, foreign_key: true

      t.timestamps
    end

    add_index :repositories, :node_id, unique: true
  end
end
