Rails.application.routes.draw do
  get '/auth/github/callback', to: 'omniauth_callbacks#github'

  get '/auth/failure', to: 'omniauth_callbacks#failure'

  resources :repositories, only: [:index]

  resources :events, only: [:create]
end
